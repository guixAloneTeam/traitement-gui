
import java.util.ArrayList;

public class lineHough {

		ImagePerso input;
		ImagePerso output;
		ImagePerso noirBlanc;
		float[] template={-1,0,1,-2,0,2,-1,0,1};;
		double progress;
		int width;
		int height;
		int[] acc;
		int[][] accumulateurPixelTrace;
		int accSize=30;
		int[] results;
		Vector2 pointIntersectionMulti;

		public void lineHough() {
		}

		public void init(ImagePerso inputIn, int widthIn, int heightIn) {
			
			this.noirBlanc = new ImagePerso(inputIn);
			for(int i=0; i<widthIn; i++){
				for(int j=0; j<heightIn; j++){
					this.noirBlanc.img[j][i][0] = 0;
					this.noirBlanc.img[j][i][1] = 0;
					this.noirBlanc.img[j][i][2] = 0;
				}
			}
			
			System.out.println("Detection ligne sur img de taille " + widthIn + " x "+ heightIn );
			this.width=widthIn;
			this.height=heightIn;
			this.input = inputIn;
			this.output = new ImagePerso(inputIn);

		}
		public void setLines(int lines) {
			accSize=lines;		
		}
		// hough transform for lines (polar), returns the accumulator array
		public ImagePerso process() {
	
			// for polar we need accumulator of 180degress * the longest length in the image
			int rmax = (int)Math.sqrt(width*width + height*height);
			acc = new int[rmax*180];
			accumulateurPixelTrace = new int[height][width];
			int r;
				
			for(int x=0;x<width;x++) {
				for(int y=0;y<height;y++) {
				
					if (this.input.img[y][x][0] == 255) {
					
						for (int theta=0; theta<180; theta++) {
							r = (int)(x*Math.cos(((theta)*Math.PI)/180) + y*Math.sin(((theta)*Math.PI)/180));
							if ((r > 0) && (r <= rmax))
								acc[r*180+theta] = acc[r*180+theta] + 1;
						}
					}
				}
			}
		
			// now normalise to 255 and put in format for a pixel array
			int max=0;
		
			// Find max acc value
			for(r=0; r<rmax; r++) {
				for(int theta=0; theta<180; theta++) {

					if (acc[r*180+theta] > max) {
						//System.out.println("Value :" + acc[r*180+theta] + " " + theta);
						max = acc[r*180+theta];
					}
				}
			}
		
			//System.out.println("Max :" + max);
		
			// Normalise all the values
			int value;
			for(r=0; r<rmax; r++) {
				for(int theta=0; theta<180; theta++) {

					value = (int)(((double)acc[r*180+theta]/(double)max)*255.0);
					acc[r*180+theta] = 0xff000000 | (value << 16 | value << 8 | value);
				}
			}

			//accSize=rmax;
			findMaxima();

			System.out.println("V�rification des lignes effectu�e");
			return this.output;
		}
		private void findMaxima() {
	
			// for polar we need accumulator of 180degress * the longest length in the image
			int rmax = (int)Math.sqrt(width*width + height*height);
			results = new int[accSize*3];

		
			for(int r=0; r<rmax; r++) {
				for(int theta=0; theta<180; theta++) {
					int value = (acc[r*180+theta] & 0xff);

					// if its higher than lowest value add it and then sort
					if (value > results[(accSize-1)*3]) {

						// add to bottom of array
						results[(accSize-1)*3] = value;
						results[(accSize-1)*3+1] = r;
						results[(accSize-1)*3+2] = theta;
					
						// shift up until its in right place
						int i = (accSize-2)*3;
						while ((i >= 0) && (results[i+3] > results[i])) {
							for(int j=0; j<3; j++) {
								int temp = results[i+j];
								results[i+j] = results[i+3+j];
								results[i+3+j] = temp;
							}
							i = i - 3;
							if (i < 0) break;
						}
					}
				}
			}
		
			for(int i=accSize-1; i>=0; i--){			
				//System.out.println("value: " + results[i*3] + ", r: " + results[i*3+1] + ", theta: " + results[i*3+2]);
				drawPolarLine(results[i*3], results[i*3+1], results[i*3+2]);
			}
		}
	
	
		// draw a line given polar coordinates (and an input image to allow drawing more than one line) 
		private void drawPolarLine(int value, int r, int theta) {
			//System.out.println("debut trace");
			
			Vector2 p1 = new Vector2();
			Vector2 p2 = new Vector2();
			int compteur=0;
			
			for(int x=0;x<width;x++) {
			
				for(int y=0;y<height;y++) {
				
					int temp = (int)(x*Math.cos(((theta)*Math.PI)/180) + y*Math.sin(((theta)*Math.PI)/180));
					if((temp - r) == 0)
					{
						if(compteur == 0){
							p1 = new Vector2(x,y);
						}
						if(compteur == 100){
							p2 = new Vector2(x,y);
						}
						compteur++;
						this.output.img[y][x][2] = 255;
						this.noirBlanc.img[y][x][2] = 255;
						this.accumulateurPixelTrace[y][x]+=1;
							
					}
					
					
				}
			}
			
			//On va tracer le point le plus trac� de l'accumulateur
			int indicex =0;
			int indicey=0;
			int max = -666;
			for(int i=0; i<height;i++){
				for(int j=0; j<width;j++){
					if(this.accumulateurPixelTrace[i][j] > max){
						max = this.accumulateurPixelTrace[i][j];
						indicex = i;
						indicey = j;
					}
				}
			}
			
			this.pointIntersectionMulti = new Vector2(indicex,indicey);
			if(indicex >0 && indicey>0){
				this.output.img[indicex][indicey][1] = 255;
				this.output.img[indicex][indicey-1][1] = 255;
				this.output.img[indicex-1][indicey][1] = 255;
				this.output.img[indicex+1][indicey][1] = 255;
				this.output.img[indicex][indicey+1][1] = 255;
				this.output.img[indicex][indicey-2][1] = 255;
				this.output.img[indicex-2][indicey][1] = 255;
				this.output.img[indicex+2][indicey][1] = 255;
				this.output.img[indicex][indicey+2][1] = 255;
			}
		}

		public int[] getAcc() {
			return acc;
		}

		
	}