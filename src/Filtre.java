import java.util.ArrayList;

public class Filtre {
	
	ArrayList<NoyauConvolution> noyaux = new ArrayList<>();
		
	public Filtre(){
		
	}
	
	public ImagePerso apply(ImagePerso img){		//!\\ ATTENTION CELA AUTORISE LES FILTRES AVEC DES NOYAUX DE MEME TAILLES et m�me diviseur
			
		ImagePerso newImage = new ImagePerso(img.getHeight(),img.getWidth());
		newImage.original = img.original;
		int tailleNoyau = this.noyaux.get(0).taille;  //Dans un filtre les noyaux font la m�me taille!
		int marge = tailleNoyau/2;
		double diviseur = this.noyaux.get(0).diviseur;
		
		for(int i=marge ; i<img.getHeight()-marge; i++){ //pour chaque pixel de l'image 
			for(int j=marge; j <img.getWidth()-marge ; j++){  //pour chaque pixel de l'image 
				double sum=0;
				int ligne=0;
				int colonne =0;
				ArrayList<Double> valeurImg = new ArrayList<Double>();
				for(int k=0; k <this.noyaux.size(); k++) //On applique les diff�rents noyaux un par un sur le pixel en cours pour stocker les valeurs
				{
					sum = 0;
					ligne=0;
					colonne =0;
					for(int l = i-marge ; l <i-marge+tailleNoyau ; l++){ //on se d�place dans le noyau sur l'image
						for(int m = j-marge; m<j-marge+tailleNoyau; m++){ //on se d�place dans le noyau sur l'image
							double multi = this.noyaux.get(k).noyau[ligne][colonne];
							double valImage = img.img[l][m][0];
							sum += valImage * multi;
							//on d�place dans le noyau
							colonne += 1;
							if(colonne >= tailleNoyau){
								ligne +=1;
								colonne =0;
							}
							if(ligne >= tailleNoyau)ligne =0;
						}
					}
					if(diviseur > 1){
						sum = sum / diviseur; //!\\ ATTENTION DIVISEUR UNIQUE, ON PREND LA VALEUR DANS LE PREMIER FILTRE !
					}
					valeurImg.add(sum); //on stocke la valeur d'un pixel pour chaque noyau
				}
				double pixelFinal=0;
				for(int k=0; k<valeurImg.size(); k++){
					//ici on va prendre le tableau des diff�rentes valeurs et appliquer la racine des valeurs au carr�
					pixelFinal += (valeurImg.get(k) * valeurImg.get(k));
				}
				pixelFinal = Math.sqrt(pixelFinal);

				if(pixelFinal>255)
				{
						newImage.img[i][j][0] = 255;
						newImage.img[i][j][1] = 255;
						newImage.img[i][j][2] = 255;
				}
				else
				{
					
					if(pixelFinal<0){
						newImage.img[i][j][0] = 0;
						newImage.img[i][j][1] = 0;
						newImage.img[i][j][2] = 0;
					}
					else{
						//System.out.println(sum);
						newImage.img[i][j][0] = pixelFinal;   
						newImage.img[i][j][1] = pixelFinal;
						newImage.img[i][j][2] = pixelFinal;
					}
				}
			}
		}
		
		
	
		return newImage;
		
	}
	
	
	
}
