import java.util.ArrayList;

public class Sobel extends Filtre{
	
	public Sobel(){
		//this.diviseur =1;
		int Gx[][] = {{-1,0,1} , {-2,0,2} , {-1,0,1} };
		int Gy[][] = {{-1,-2,-1} , {0,0,0} , {1,2,1} };
		this.noyaux.add(new NoyauConvolution(3,1,Gx));
		this.noyaux.add(new NoyauConvolution(3,1,Gy));
	}
	
	
	public ImagePerso applyBrut(ImagePerso img){	//pour appliquer de mani�re brut le filtre! (aucune gestion de la taille des noyaux etc
			
			ImagePerso newImage = new ImagePerso(img.getWidth(),img.getHeight());
			newImage.original = img.original;
			int taille_noyau = 3;
			int marge = taille_noyau/2;
			double sum =0;
			double sum2 =0;
			double tmpSum = 0;
											
			for(int i=marge ; i<img.getHeight()-marge; i++){ //pour chaque pixel de l'image 
				for(int j=marge; j <img.getWidth()-marge ; j++){  //pour chaque pixel de l'image 
								
					sum = 0 ;	
					sum2 =0;
					int ligne =0; 
					int colonne =0;
					
					for(int k = i-marge ; k <i-marge+taille_noyau ; k++){ //on se d�place dans le noyau sur l'image
						for(int l = j-marge; l<j-marge+taille_noyau; l++){
							tmpSum = img.img[k][l][0];		
							
							double multi = this.noyaux.get(0).noyau[ligne][colonne];   //tab[lignes][colonnes]
							double multi2 = this.noyaux.get(1).noyau[ligne][colonne]; 
							sum += tmpSum * multi;
							sum2 += tmpSum * multi2;
							
							colonne += 1;
							if(colonne >= taille_noyau){
								ligne +=1;
								colonne =0;
							}
							if(ligne >= taille_noyau)ligne =0;
							
							
						}
					} //fin d�placement dans le noyau
					
					double sumFinal = Math.sqrt((sum*sum)+(sum2*sum2));
					ligne=0;
					colonne=0;								
					 
					
					if(sumFinal>255)
					{
							newImage.img[i][j][0] = 255;
							newImage.img[i][j][1] = 255;
							newImage.img[i][j][2] = 255;
					}
					else
					{
						
						if(sumFinal<0){
							newImage.img[i][j][0] = 0;
							newImage.img[i][j][1] = 0;
							newImage.img[i][j][2] = 0;
						}
						else{
							//System.out.println(sum);
							newImage.img[i][j][0] = sumFinal;   
							newImage.img[i][j][1] = sumFinal;
							newImage.img[i][j][2] = sumFinal;
						}
					}
																
									
										
				} //fin pour 
							
			}  //fin pour 
					
	return newImage;

	}
}
