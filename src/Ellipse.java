
public class Ellipse {
	Vector2 centre;
	double alpha; //orientation
	double a;  //major axe
	double b; //minor axe
	int vote=0;
	//5 pixels de bords servent souvent pour calculer les paramÍtres
	
	public Ellipse(){
		
	}
	
	public Ellipse(Vector2 centre, double a, double b, double alpha){
		this.centre = centre;
		this.alpha=alpha;
		this.a = a;
		this.b = b;
	}
	
}
